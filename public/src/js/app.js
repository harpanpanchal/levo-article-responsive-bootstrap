// jquery ready start

$(document).ready(function () {
  $(function () {
    $(
      '.block-3-column-image-and-text__list .block-3-column-image-and-text__list--item'
    ).hide();
    $(
      '.block-3-column-image-and-text__list .block-3-column-image-and-text__list--item'
    )
      .slice(0, 3)
      .show();
    $('#loadMore').on('click', function (e) {
      e.preventDefault();
      $(
        '.block-3-column-image-and-text__list .block-3-column-image-and-text__list--item:hidden'
      )
        .slice(0, 3)
        .slideDown();
      if (
        $(
          '.block-3-column-image-and-text__list .block-3-column-image-and-text__list--item:hidden'
        ).length == 0
      ) {
        $(this).hide();
      }
    });
  });

  // Comparison Table Js starts here

  $('.account-modalities article').each((index, element) => {
    $(element).on('click touchend', (e) => {
      e.preventDefault();
      $('.account-modalities table tr td')
        .add('.account-modalities article')
        .removeClass('active');
      $(`.account-modalities table tr td:nth-child(${(index + 1) * 2})`)
        .add(element)
        .addClass('active');
    });
  });

  // Comparison Table Js ends here

  $('html[lang=en]')
    .attr('dir', 'ltr')
    .find('head')
    .append(
      "<link rel='stylesheet' type='text/css' href='dist/css/app.min.css' />"
    );
}); // jquery end
