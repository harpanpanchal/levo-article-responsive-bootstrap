# Installation

1. 'npm install' to install all the dependencies.
2. run 'gulp watch' command to run the project in localhost.
3. We also need to run 'gulp build' command to build the production folder. i.e. 'dist'.
4. Once done, run 'gulp watch' again to view the main page in localhost.
5. 'gulp clean' will delete 'dist' folder'. We'd need to run the 'gulp build' folder to view the style related changes in the browser as the css has been fetched from the 'dist' folder.
6. 'Prettier' has been used locally in visual studio. Not added the package for the same but, can be added as dependencies if needed.

# Important points

1. Sass (.scss) files will be found in public -> src -> scss folder.
2. JS file will be found in public -> src -> js folder.
3. Please refer gulpfile.js and package.json file for the configuration.
